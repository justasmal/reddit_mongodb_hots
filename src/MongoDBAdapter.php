<?php


namespace App;


use MongoDB\Client;
use MongoDB\Collection;

class MongoDBAdapter
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * MongoDBAdapter constructor.
     * @param string $uri
     * @param string $db
     * @param string $collection
     */
    public function __construct(string $uri, string $db, string $collection) {
        $this->client = (new Client($uri))->$db->$collection;
    }

    /**
     * @return Client|Collection
     */
    public function getClient() {
        return $this->client;
    }
}