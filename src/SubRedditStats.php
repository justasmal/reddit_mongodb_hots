<?php


namespace App;


use \Exception;

class SubRedditStats
{
    private MongoDBAdapter $adapter;

    public function __construct(MongoDBAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getListSubreddits()
    {
        $pipeline = [
            [
                '$group' => [
                    '_id' => [
                        "subreddit" => '$subreddit',
                        "prefixed" => '$subreddit_name_prefixed'
                    ]

                ]
            ]
        ];

        try {
            $data = $this->adapter->getClient()->aggregate($pipeline);
        } catch (Exception $exception) {
            return [];
        }

        return $data;
    }

    public function getMostUpvoted($subreddit = null, $limit = 10)
    {
        $find = [];
        $ops = [
            'limit' => $limit,
            'sort' => ['ups' => -1],
        ];
        if ($subreddit !== null) {
            $find = ["subreddit" => $subreddit];
        }
        $data = $this->adapter->getClient()->find($find, $ops

        );
        return $data->toArray();
    }
}