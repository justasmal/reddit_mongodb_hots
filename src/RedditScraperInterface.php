<?php

namespace App;

interface RedditScraperInterface
{
    public function scrape($subreddit);
}