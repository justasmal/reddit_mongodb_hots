<?php
namespace App;

use Exception;

class PushSubreddits {
    private static function getConfigFile()
    {
        $data = file_get_contents("settings.json");
        $data = json_decode($data);
        if ($data === false || $data === null) {
            throw new Exception("Failed to read config file");
        }
        return $data;
    }

    public static function push(): bool
    {
        $val = false;
        try {
            $settings = self::getConfigFile();
            $data = file_get_contents("subreddits.json");
            $data = json_decode($data, true);
            $queue = new MongoQueue($settings->mongouri, $settings->database, $settings->queuecollection);
            foreach ($data as $subreddit) {
                $queue->publish(["subreddit" => $subreddit]);
            }
            $val = true;
            $count = count($data);
            echo "Pushed $count subreddits\n";
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
        return $val;
    }

    public static function makeIndexes()
    {
        try {
            $settings = self::getConfigFile();
            $mongo = new MongoDBAdapter($settings->mongouri, $settings->database, $settings->collection);
            $mongo->getClient()->createIndex(["subreddit" => 1]);
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
}