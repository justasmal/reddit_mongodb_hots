<?php


namespace App;

use MongoDB\BSON\UTCDateTime;

class MongoQueue extends MongoDBAdapter
{
    /**
     * MongoQueue constructor.
     * @param string $uri
     * @param string $db
     * @param string $collection
     * @param int $expireAfter
     */
    public function __construct(string $uri, string $db, string $collection, $expireAfter = 1200)
    {
        parent::__construct($uri, $db, $collection);
        $this->client->createIndex(["createdAt" => 1], ["expireAfterSeconds" => $expireAfter]);
    }

    /**
     * Adds item to the queue if the message doesnt exist yet
     * @param $data
     */
    public function publish($data)
    {
        $element = $this->client->findOne(["message" => $data]);
        if ($element === null) {
            $time = new UTCDateTime(time() * 1000);
            $this->client->insertOne(
                [
                    "createdAt" => $time,
                    "isProcessing" => false,
                    "message" => $data
                ]
            );
        }
    }

    /**
     * Looks for items in queue and marks it as in process
     * @return array|object|null
     */
    public function listen()
    {
        $element = $this->client->findOne(
            [
                "isProcessing" => false
            ],
            [
                "sort" => [
                    'createdAt' => 1
                ]
            ]
        );
        if ($element !== null) {
            $filterOption = ["_id" => $element["_id"]];
            $updateOption = [ 'isProcessing' => true];
            $this->client->updateOne(
                $filterOption,
                ['$set' => $updateOption]
            );
            return $element;
        }
        return null;
    }

    /**
     * Marks item as completed
     * @param $element
     * @return bool
     */
    public function ack($element): bool
    {
        $element = $this->client->deleteOne(
            [
                "_id" => $element["_id"]
            ]
        );
        return $element->isAcknowledged();
    }
}