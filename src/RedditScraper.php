<?php

namespace App;

use Exception;
use JsonPath\JsonObject;

class RedditScraper implements RedditScraperInterface
{
    public static function get($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function scrape($subreddit)
    {
        $output = self::get("https://reddit.com/r/$subreddit.json?limit=100");
        $jsonObject = new JsonObject($output);
        $posts = $jsonObject->get("$.data.children[*][data]");
        if(is_array($posts)) {
            return $posts;
        } else {
            throw new Exception("Posts not found");
        }
    }
}