<?php

require_once __DIR__ . "/vendor/autoload.php";

use App\MongoDBAdapter;
use App\RedditScraper;


$scaper = new RedditScraper();
$posts = $scaper->scrape("linux");
$mongouri = "mongodb://root:example@127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000";

$mongo = new MongoDBAdapter($mongouri, "reddit_db", "reddit");

$cursor = $mongo->getClient()->find([]);
$updated = 0;
foreach ($posts as $document) {
    $updateResult =  $mongo->getClient()->updateOne(
        ['id' => $document['id']],
        ['$set' => $document]
    );
    $updated += $updateResult->getModifiedCount();
}
$size = count($posts);
echo "Updated $updated of $size posts scraped";
