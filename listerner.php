<?php

require_once __DIR__ . "/vendor/autoload.php";

use App\MongoDBAdapter;
use App\MongoQueue;
use App\RedditScraper;
use React\EventLoop\Factory;

$loop = Factory::create();

$settings = getConfigFile();
function getConfigFile()
{
    $data = file_get_contents("settings.json");
    $data = json_decode($data);
    if ($data === false || $data === null) {
        throw new Exception("Failed to read config file");
    }
    return $data;
}

$queue = new MongoQueue($settings->mongouri, $settings->database, $settings->queuecollection);
$mongo = new MongoDBAdapter($settings->mongouri, $settings->database, $settings->collection);
$mongo->getClient()->createIndex(["id" => 1], ["unique" => true]);
$loop->addPeriodicTimer(1, function () use ($queue, $mongo) {
    $timer = time();
    $element = $queue->listen();
    if ($element !== null) {
        try {
            $scaper = new RedditScraper();
            $subreddit = $element['message']['subreddit'];
            $posts = $scaper->scrape($subreddit);

            $updated = 0;
            foreach ($posts as $document) {
                $updateResult = $mongo->getClient()->replaceOne(
                    ['id' => $document['id']],
                    $document
                );
                if ($updateResult->getMatchedCount() === 0) {
                    $mongo->getClient()->insertOne($document);
                }
                $updated += $updateResult->getModifiedCount();
            }
            $size = count($posts);
            $added = $size - $updated;
            echo "Updated $updated and inserted $added of $size posts scraped for Subreddit $subreddit\n";
            $queue->ack($element);
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
        $timer = time() - $timer;
        echo "Script ran in $timer seconds.\n";
    }
});

$loop->run();