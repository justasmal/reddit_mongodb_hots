<?php
require_once __DIR__ . "/vendor/autoload.php";

use App\PushSubreddits;
use React\EventLoop\Factory;
use React\Promise\PromiseInterface;
use WyriHaximus\React\Cron;
use WyriHaximus\React\Cron\Action;
use function React\Promise\resolve;


$loop = Factory::create();

$actions = [];
PushSubreddits::makeIndexes();

$action = new Action(
    "add_to_queue",
    0.1,
    '*/5 * * * *',
    function (): PromiseInterface {
        return resolve(PushSubreddits::push());
    }
);

$cron = Cron::create(
    $loop,
    $action
);


$loop->run();