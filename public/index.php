<?php

use App\MongoDBAdapter;
use App\SubRedditStats;

require_once __DIR__ . "/../vendor/autoload.php";


$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', 'index');
    $r->addRoute('GET', '/charts', 'charts');
    $r->addRoute('GET', '/getsubreddits', 'getsubreddits');
    $r->addRoute('GET', '/getmostupvoted[/{subreddit}]', 'getmostupvoted');
    $r->addRoute('GET', '/gettop10', 'gettop10');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        whatToDo($handler, $vars);
        break;
}

function whatToDo($handler, $vars)
{
    switch ($handler) {
        case "index":
            include_once "view.html";
            break;
        case "getsubreddits":
            header('Content-type: application/json');
            $stats = getsubreddits();
            echo json_encode($stats);
            break;
        case "getmostupvoted":
            header('Content-type: application/json');
            $stats = getmostupvoted($vars);
            echo json_encode($stats);
            break;
        default:
            var_dump($handler, $vars);
            break;
    }
}

function getsubreddits(): array
{
    $subredditStats = new SubRedditStats(getMongo());
    $subreddits = [];
    foreach ($subredditStats->getListSubreddits() as $listSubreddit) {
        $subreddits[] = $listSubreddit['_id'];
    }
    return $subreddits;

}

function getmostupvoted($vars): array
{
    $subreddit = $vars["subreddit"] ?? null;
    $subredditStats = new SubRedditStats(getMongo());
    return $subredditStats->getMostUpvoted($subreddit, 10);

}
function getConfigFile()
{
    $data = file_get_contents("../settings.json");
    $data = json_decode($data);
    if ($data === false || $data === null) {
        throw new Exception("Failed to read config file");
    }
    return $data;
}
function getMongo() {
    $settings = getConfigFile();
    return new MongoDBAdapter($settings->mongouri, $settings->database, $settings->collection);
}